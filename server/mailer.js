import connect from 'connect'
import bodyParser from 'body-parser'
import { createTransport } from 'nodemailer'
import { isEmail } from 'validator'
import { inHTMLData } from 'xss-filters'

const app = connect()

app.use(bodyParser.json())

const validateAndSanitize = (key, value) => {
  const rejectFunctions = {
    name: v => v.length < 4,
    email: v => !isEmail(v),
    msg: v => v.length < 25,
  }

  // If object has key and function returns false, return sanitized input.
  // Else, return false
  return (
    // eslint-disable-next-line
    rejectFunctions.hasOwnProperty(key) &&
    !rejectFunctions[key](value) &&
    inHTMLData(value)
  )
}

const sendMail = (_name, email, msg) => {
  const transporter = createTransport({
    sendmail: true,
    newline: 'unix',
    path: '/usr/sbin/sendmail',
  })
  transporter.sendMail({
    from: email,
    to: 'hola@wdinamo.com',
    subject: 'New contact form message',
    text: msg,
  })
}

app.use('/', (req, res) => {
  if (req.method !== 'post') {
    return res.status(405).json({ error: 'sorry!' })
  }
  const attributes = ['name', 'email', 'msg']
  const sanitizedAttributes = attributes.map(n =>
    validateAndSanitize(n, req.body[n])
  )
  const someInvalid = sanitizedAttributes.some(r => !r)

  if (someInvalid) {
    return res.status(422).json({ error: 'Ugh.. That looks unprocessable!' })
  }

  sendMail(...sanitizedAttributes)
  return res.status(200).json({ message: 'OH YEAH' })
})

export const path = '/api/contact'
export const handler = app
