/**
 * @description Throtle, ejecuta algo solo una vez cada x tiempo
 *
 * @export
 * @param {function} fn La función a ejecutar
 * @param {Number} wait Tiempo en milisegundos
 * @returns {Void} Void
 */
export function throttle(fn, wait) {
  let time = Date.now()
  return () => {
    if (time + wait - Date.now() < 0) {
      fn()
      time = Date.now()
    }
  }
}

/**
 * @description Procesa opciones que puede ser una funcion o un objeto
 *
 * @export
 * @param {Object} value La función o objeto
 * @returns  {Object} El objeto o el resultado del callback
 */
export function processOptions(value) {
  let options
  if (typeof value === 'function') {
    // Simple options (callback-only)
    options = {
      callback: value,
    }
  } else {
    // Options object
    options = value
  }
  return options
}

/**
 * @description Compara dos objetos de forma recursiva
 *
 * @export
 * @param {Object} obj1 El primero objeto a comparar
 * @param {Object} obj2 El segundo objeto a comparar
 * @returns {Boolean} Si en efecto los dos objetos son iguales
 */
export function deepEqual(obj1, obj2) {
  if (obj1 === obj2) return true
  if (typeof obj1 === 'object') {
    const keys = Object.keys(obj1)
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i]
      if (!deepEqual(obj1[key], obj2[key])) {
        return false
      }
    }
    return true
  }
  return false
}

/**
 * @description Genera un número aleatorio
 *
 * @export
 * @returns {Number} - Un número aleatorio
 */
export function randomNumber() {
  return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1)
}

/**
 * Un Comportamiento de scroll donde automáticamente hace scroll
 * a la ruta hija más profunda posible.
 * @param {Route} to - De donde venimos
 * @param {Route} from - A Donde vamos
 * @param {Number} savedPosition - La posición previa (back)
 *
 * @returns {{Promise}} La posición en la pantalla
 */
const scrollBehavior = (to, from, savedPosition) => {
  // if the returned position is falsy or an empty object,
  // will retain current scroll position.
  let position = false
  let scrollToChild = false

  // if no children detected
  if (to.matched.length < 2) {
    // scroll to the top of the page
    position = {
      x: 0,
      y: 0,
    }
  } else if (to.matched.some(r => r.components.default.options.scrollToTop)) {
    // if one of the children has scrollToTop option set to true
    position = {
      x: 0,
      y: 0,
    }
  } else {
    scrollToChild = true
  }

  // savedPosition is only available for popstate navigations (back button)
  if (savedPosition) {
    position = savedPosition
  }

  return new Promise(resolve => {
    // wait for the out transition to complete (if necessary)
    window.$nuxt.$once('triggerScroll', () => {
      // coords will be used if no selector is provided,
      // or if the selector didn't match any element.
      if (to.hash && document.querySelector(to.hash)) {
        // scroll to anchor by returning the selector
        position = {
          selector: to.hash,
        }
      } else if (scrollToChild) {
        // Always scroll to child components if there is one
        const lastMatch = [...to.matched].pop()
        const lastChild = lastMatch.instances.default.$el
        const { top } = lastChild.getBoundingClientRect()
        position = {
          x: 0,
          y: Math.max(0, top + window.scrollY - 100),
        }
        window.console.log({
          lastChild,
          position,
          top,
        })
      }
      resolve(position)
    })
  })
}

/**
 * @description Returns a function, that, as long as it continues to be invoked,
 * will not be triggered. The function will be called after it stops being
 * called for N milliseconds. If `immediate` is passed, trigger the function on
 * the leading edge, instead of the trailing.
 *
 * @export
 * @param {Function} func La función a ejecutar
 * @param {Number} wait El tiempo a esperar
 * @param {inmedia} immediate Ejecutar inmediatamente
 * @returns {Function} La misma funcion que se dio como argumento
 */
export function debounce(func, wait, immediate) {
  let timeout
  return (...args) => {
    const context = this

    const later = () => {
      timeout = null
      if (!immediate) func.apply(context, args)
    }
    const callNow = immediate && !timeout
    clearTimeout(timeout)
    timeout = setTimeout(later, wait)
    if (callNow) func.apply(context, args)
  }
}

export default { throttle, processOptions, deepEqual, scrollBehavior }
