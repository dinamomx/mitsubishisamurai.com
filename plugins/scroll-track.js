import { debounce } from '~/utils'

/**
 * Monitorea el nivel de scroll del usuario y añade un evento
 * de analíticas cuando sobrepasa la mitad de la página
 */

class ScrollTrack {
  currentScrollPercentage = 0

  tracked = 0

  latestPage = ''

  currentPage = ''

  scrollTrigger = 50

  /**
   * Empieza a monitorear el scroll de la página
   * @returns {Void} - Nothing
   */
  init() {
    window.addEventListener('scroll', debounce(this.watch.bind(this), 500))
  }

  destroy() {
    window.removeEventListener('scroll', debounce(this.watch.bind(this), 500))
  }

  /**
   * Aplica la diferencia del alto de la pantalla con el scroll actual
   * y envía un evento de anaíticas cuando se sobrepasa la mitad
   * @returns {Void} - Nothing
   */
  watch() {
    const scrollTop = window.scrollY
    const docHeight = document.body.offsetHeight
    const winHeight = window.innerHeight
    const scrollPercent = scrollTop / (docHeight - winHeight)
    const scrollPercentRounded = Math.round(scrollPercent * 100)
    this.currentScrollPercentage = scrollPercentRounded
    if (this.tracked < scrollPercentRounded) {
      this.tracked = scrollPercentRounded
      if (
        scrollPercentRounded > this.scrollTrigger &&
        scrollPercentRounded % 10 === 0
      ) {
        this.trackScrollDepth()
      }
    }
  }

  trackScrollDepth() {
    if (typeof window.ga !== 'undefined') {
      window.ga('send', {
        hitType: 'event',
        eventCategory: 'user-tracking',
        eventAction: 'Scroll Depth',
        eventLabel: document.title,
        eventValue: this.tracked,
      })
    } else {
      // eslint-disable-next-line no-console
      console.warn('Analítics no está instalado', this)
    }
    window.$nuxt.$emit('scrollTo', this.tracked)
  }
}

export default ({ app }) => {
  const scrollTrack = new ScrollTrack()
  app.router.afterEach(to => {
    scrollTrack.latestPage = scrollTrack.currentPage
    scrollTrack.currentPage = to
    scrollTrack.tracked = 0
  })
  if (typeof document !== 'undefined') {
    scrollTrack.currentPage = document.location.pathname
    scrollTrack.init()
  }
}
