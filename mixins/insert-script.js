/* eslint valid-jsdoc: ["error", { "requireReturn": false }] */

/**
 * Inserta scripts "tradicionales" a la página y mantiene sincronizados los
 * eventos para lograr esto, elimina e inserta de nuevo el script.
 * Tiene que personalizarse de acuerdo a cada uso.
 * Está pensado para usarse en un layout.
 * @mixin
 *
 */
export default {
  data: () => ({
    scriptElementement: null,
    scriptName: 'custom',
    scriptSource: '//cdn.scriptsource.com/static/plugin.js',
  }),
  watch: {
    $route() {
      this.reloadScript()
    },
  },
  mounted() {
    this.insertScript()
  },
  beforeDestroy() {
    // asegurate de quitarlo si llegamos a cambiar de layout
    this.destroyScript()
  },
  methods: {
    /**
     * @description Inserta el script en la página
     *
     * @returns {Node} El padre en el cual se insertó el script
     */
    insertScript() {
      // Crea el elemento
      // mantén una referencia local al elemento
      this.scriptElement = document.createElement('script')
      this.scriptElement.type = 'text/javascript'
      this.scriptElement.async = true
      this.scriptElement.id = `${this.scriptName}-script`
      this.scriptElement.src = `${this.scriptSource}?v=${+new Date()}`
      // inserta el elemento
      return document.body.appendChild(this.scriptElement)
    },
    /**
     * Función para recargar el script de booking.com y que pueda cargar
     * los widgets sin importar el cambio de página.
     * @returns {Boolean} - Si se recargó o no
     */
    reloadScript() {
      // browser only
      if (!process.browser) {
        return false
      }
      // Si ya existía el elemento, mandalo a la chingada
      this.destroyScript()
      this.insertScript()
      return true
    },
    /**
     * Destruye el script y sus elementos globales en la ventana
     */
    destroyScript() {
      if (this.scriptElement) {
        // quita el elemento
        this.scriptElement.remove()
        this.scriptElement = null
        // Elimina las referencias creadas por el script (Manual)
        // delete window.BookingAff
      }
    },
  },
}
